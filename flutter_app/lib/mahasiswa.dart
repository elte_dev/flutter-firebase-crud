import 'package:firebase_database/firebase_database.dart';

class Mahasiswa {

  String _id;
  String _nama;
  String _email;
  String _usia;
  String _hp;

  Mahasiswa(this._id,this._nama, this._email, this._usia, this._hp);

  String get nama => _nama;

  String get email => _email;

  String get usia => _usia;

  String get hp => _hp;

  String get id => _id;

  Mahasiswa.fromSnapshot(DataSnapshot snapshot) {
    _id = snapshot.key;
    _nama = snapshot.value['nama'];
    _email = snapshot.value['email'];
    _usia = snapshot.value['usia'];
    _hp = snapshot.value['hp'];
  }
}